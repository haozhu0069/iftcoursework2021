# Data preprocessing
# Merge relevant data
portfolio_positions_merged <- merge(equity_static[c("symbol", "GICSSector")],
                                    portfolio_positions[c("trader", "cob_date", "symbol", "net_quantity", "net_amount", "ccy")],
                                    by="symbol",
                                    no.dups = TRUE)

names(equity_prices)[names(equity_prices) == "symbol_id"] <- "symbol"

portfolio_positions_merged <- merge(equity_prices[c("symbol", "cob_date", "close")],
                                    portfolio_positions_merged,
                                    by=c("symbol", "cob_date"),
                                    no.dups = TRUE)

# Calculate MtM & PnL
portfolio_positions_merged <- portfolio_positions_merged %>%
  group_by(cob_date, trader, symbol) %>%
  mutate(MtM = net_quantity * close)

portfolio_positions_merged <- portfolio_positions_merged %>%
  group_by(cob_date, trader, symbol) %>%
  mutate(PnL = MtM - net_amount)

# Aggregate by GICSSector------------------------------------------------
portfolio_positions_sec <- aggregate(cbind(net_quantity, net_amount, MtM, PnL)~cob_date+trader+GICSSector, 
                                     data=portfolio_positions_merged, 
                                     FUN=sum)

# Calculate exposures by sector
# net exposures
#portfolio_positions_exp <- portfolio_positions_sec %>%
#  group_by(cob_date, trader) %>%
#  mutate(exposures_net = net_amount / sum(net_amount))
# MtM exposures
portfolio_positions_exp <- portfolio_positions_sec %>%
  group_by(cob_date, trader) %>%
  mutate(exposures_MtM = MtM / sum(MtM))

# Rounding
portfolio_positions_merged <- portfolio_positions_merged %>% 
  mutate(across(where(is.numeric), round, 2))

portfolio_positions_exp <- portfolio_positions_exp %>% 
  mutate(across(where(is.numeric), round, 4))

# Check whether exposures sum up to be 1
#portfolio_positions_check <- aggregate(exposures_MtM~cob_date+trader, 
#                                       data=portfolio_positions_exp, 
#                                       FUN=sum)


# Reorder Processed Data
# portfolio_positions_merged
colnames(portfolio_positions_merged)
portfolio_positions_merged <- portfolio_positions_merged[, c("cob_date",
                                                             "trader",
                                                             "symbol",
                                                             "net_quantity",
                                                             "net_amount",
                                                             "MtM",
                                                             "PnL",
                                                             "GICSSector",
                                                             "ccy")]
portfolio_positions_merged <- portfolio_positions_merged[with(
  portfolio_positions_merged, order(cob_date, trader)
  ),]
# portfolio_positions_exp
colnames(portfolio_positions_exp)
portfolio_positions_exp <- portfolio_positions_exp[, c("cob_date",
                                                       "trader",
                                                       "GICSSector",
                                                       "exposures_MtM")]

portfolio_positions_exp <- portfolio_positions_exp[with(
  portfolio_positions_exp, order(cob_date, trader)
  ),]

portfolio_positions_sec <- portfolio_positions_sec[with(
  portfolio_positions_sec, order(cob_date, trader)
  ),]

# Input choices
#unique(portfolio_positions_sec$trader)
#unique(portfolio_positions_sec$cob_date)
