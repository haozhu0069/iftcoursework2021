//Coursework1 --NoSQLQueries
//Auther:Yin Duan

//Query1 
//This query is to show all securities in Pharmaceuticals subIndustry.
  db.CourseworkOne.find({'StaticData.GICSSubIndustry':'Pharmaceuticals'},{'StaticData.Security':1}).pretty()

//Query2
//This query is to find the securities that hold the PERation greater than 20 and smaller than 30 and then arrange them by decending order of the MarketCap.Finally the results return the top 5 securities with the largest market capital.
db.CourseworkOne.find({$and:[{'FinancialRatios.PERatio':{'$gte':20}},{'FinancialRatios.PERatio':{'$lt':30}}]}).limit(5).sort({'MarketData.MarketCap':-1})

//Query3
//This query is to use aggregate function to calculate the averageBeta of all the securities with price greater than 50 in every sector.
db.CourseworkOne.aggregate([
{$match: {'MarketData.Price':{'$gte':50}} },
{$group: {_id: "$StaticData.GICSSector", averageBeta: {$avg: "$MarketData.Beta"} } }])