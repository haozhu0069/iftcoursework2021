#--------------------------------------------------------------------------------------
# Coursework2
# Author  : Yin Duan
# Topic   : Data Preprocessing
#--------------------------------------------------------------------------------------

#Aggregate all trades by Trader, Symbol, Ccy and Date
#The coding below is to transform DateTime to Date
for (i in 1:nrow(Data_main)) {
  if(grepl('2021-11-11',Data_main$DateTime[i])==T){
    Data_main$DateTime[i] <- '11-11-2021'
  }else{
    Data_main$DateTime[i] <- '12-11-2021'
  }
  
}
names(Data_main)[names(Data_main)=='DateTime'] <- 'Date'
Data_main$Date <- as.Date(Data_main$Date,'%d-%m-%Y')

#This is to get the aggregated data of 2021-11-11 & 2021-11-12
Data_1112 <- aggregate(Data_main[c('Quantity','Notional')],by=Data_main[c('Trader','Symbol','Ccy','Date')],FUN = sum)

#Get the mark to market position of each security

Equity_Price$cob_date <- as.Date(Equity_Price$cob_date,'%d-%b-%Y')
for (i in 1:nrow(Data_1112)) {
  price <- filter(Equity_Price,Equity_Price$symbol_id==Data_1112$Symbol[i],Equity_Price$cob_date==Data_1112$Date[i])$close
  Data_1112$amount[i] <- Data_1112$Quantity[i]*price
}


