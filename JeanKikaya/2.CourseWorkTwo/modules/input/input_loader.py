# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 18 16:43:43 2021

@author: Jean Ali Useni Kikaya
"""
#input loader
import pymongo
from pymongo import MongoClient
import pandas as pd


#SQL queries for low/high ranges
EquityPriceRange_nov_11 = pd.read_sql_query(SQLQueryPrices_11(),engine)
EquityPriceRange_nov_12 = pd.read_sql_query(SQLQueryPrices_12(),engine)

# mongoDB queries for November 11 and November 12
nov_11 = collection.find({"$and" : [{"DateTime" : {"$gte" : "ISODate(2021-11-11T00:00:00.000Z)"}}, {"DateTime" : {"$lte" : "ISODate(2021-11-12T00:00:00.000Z)"}}]})
nov_12 = collection.find({"$and" : [{"DateTime" : {"$gte" : "ISODate(2021-11-12T00:00:00.000Z)"}}, {"DateTime" : {"$lte" : "ISODate(2021-11-13T00:00:00.000Z)"}}]})

# dates for pos_id column
date_nov_11 = '20211111'
date_nov_12 = '20211112'

