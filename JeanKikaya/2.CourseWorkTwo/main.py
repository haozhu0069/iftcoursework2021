# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 13:39:27 2021

@author: Jean Ali Useni Kikaya
"""

import pymongo
from pymongo import MongoClient
import pandas as pd
import numpy as np
from sqlalchemy import create_engine
import pandas as pd
import numpy as np
from CreateTableSQL import SQLCreateSuspectTable #if import doesn't work, run script manually

#SQL Queries for November 11 and 12
def SQLQueryPrices_11():
    return ("SELECT low,high,symbol_id,cob_date FROM equity_prices WHERE cob_date = '11-Nov-2021' ")

def SQLQueryPrices_12():
    return ("SELECT low,high,symbol_id,cob_date FROM equity_prices WHERE cob_date = '12-Nov-2021' ")

#connection to DBs
GITRepoDirectory = "C:/Users/aliki/OneDrive/Documents/git"
engine = create_engine(f"sqlite:///{GITRepoDirectory}/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine.connect()
# import price ranges from SQL
EquityPriceRange_nov_11 = pd.read_sql_query(SQLQueryPrices_11(),engine)
EquityPriceRange_nov_12 = pd.read_sql_query(SQLQueryPrices_12(),engine)

#MongoDB connection
cluster = MongoClient("mongodb://127.0.0.1:27017")
db = cluster["Equity"]
collection = db["CourseworkTwo"]

# Queries for each date
nov_11 = collection.find({"$and" : [{"DateTime" : {"$gte" : "ISODate(2021-11-11T00:00:00.000Z)"}}, {"DateTime" : {"$lte" : "ISODate(2021-11-12T00:00:00.000Z)"}}]})
nov_12 = collection.find({"$and" : [{"DateTime" : {"$gte" : "ISODate(2021-11-12T00:00:00.000Z)"}}, {"DateTime" : {"$lte" : "ISODate(2021-11-13T00:00:00.000Z)"}}]})


#Transfering data into a dataframe
nov_11_df = pd.DataFrame(nov_11)
nov_11_df = nov_11_df.rename(columns={"Symbol": "symbol_id"})

nov_12_df = pd.DataFrame(nov_12)
nov_12_df = nov_12_df.rename(columns={"Symbol": "symbol_id"})

#calculating equity prices for each trade

date_nov_11 = '20211111'
date_nov_12 = '20211112'

nov_11_df["TradePrice"] = (nov_11_df["Notional"]/nov_11_df["Quantity"])
nov_11_df['pos_id'] = nov_11_df['Trader'].astype(str) + date_nov_11 + nov_11_df['symbol_id'].astype(str)

nov_12_df["TradePrice"] = (nov_12_df["Notional"]/nov_12_df["Quantity"])
nov_12_df['pos_id'] = nov_12_df['Trader'].astype(str) + date_nov_12 + nov_12_df['symbol_id'].astype(str)

#merging sql data with mongoDB data
nov_11_table = pd.merge(nov_11_df,EquityPriceRange_nov_11,on='symbol_id')
nov_12_table = pd.merge(nov_12_df,EquityPriceRange_nov_12,on='symbol_id')
total = pd.concat([nov_11_table,nov_12_table])

#filter out suspect trade by seeing if they are outside the low/high range
trades_suspects = total.loc[(total['TradePrice'] < total['low']) | (total['TradePrice'] > total['high'])]
trades_suspects.index = range(len(trades_suspects))
trades_suspects

#sql table creation and inserting data
con.execute(SQLCreateSuspectTable())

#inserting suspect trades into a new sql table 
for i in range(len(trades_suspects)):
    print('[INFO] Loading into trades_suspects',trades_suspects.loc[i,'TradeId'],'...\n')
    con.execute(f'INSERT INTO trades_suspects (TradeId,Quantity,Notional,symbol_id,TradePrice,Trader) \
                VALUES ("{trades_suspects.loc[i,"TradeId"]}",\
                {trades_suspects.loc[i,"Quantity"]},\
                {trades_suspects.loc[i,"Notional"]},\
                "{trades_suspects.loc[i,"symbol_id"]}",\
                {trades_suspects.loc[i,"TradePrice"]},\
                "{trades_suspects.loc[i,"Trader"]}")')
                    
# renaming columns to match portfolio_positions
total_table_update = total.rename(columns={"Ccy": "ccy","Trader": "trader","symbol_id": "symbol","Quantity": "net_quantity","Notional": "net_amount"})

#aggregate Quantity and Notional for all trades by date, trader, symbol, ccy and insert the results into portfolio_positions.
new_positions = total_table_update.groupby(['pos_id','cob_date','trader','symbol','ccy']).agg(net_quantity=('net_quantity','sum'),net_amount=('net_amount','sum')).reset_index()


# insert new aggreagted positions into portfolio_positions table
for i in range(len(new_positions)):
    print('[INFO] Loading into portfolio_positions',new_positions.loc[i,'pos_id'],'...\n')
    con.execute(f'INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) \
                VALUES ("{new_positions.loc[i,"pos_id"]}",\
                "{new_positions.loc[i,"cob_date"]}",\
                "{new_positions.loc[i,"trader"]}",\
                "{new_positions.loc[i,"symbol"]}",\
                "{new_positions.loc[i,"ccy"]}",\
                {new_positions.loc[i,"net_quantity"]},\
                "{new_positions.loc[i,"net_amount"]}")')

# switch off connection
con.close()