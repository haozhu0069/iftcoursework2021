#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Gechang Shao
# Topic   : Create shiny page - server
#--------------------------------------------------------------------------------------

server <- function(input, output) {

# Subset reactive data for portfolio positions ----------------------------

  data1 <- reactive({
    subset(
      PositionsWithIndustry,
      trader_name == input$trader &
        date == as.POSIXct(input$date) ,
      select = c(
        "date",
        "trader_name",
        "trader_id",
        "symbol",
        "security",
        "GICSSector",
        "GICSIndustry",
        "fund_currency",
        "net_quantity",
        "amount"
      )
    )
  })
  
# Subset reactive data for exposure by sector -----------------------------

  data2 <- reactive({
    PositionsBySector %>%
      filter(trader_name == input$trader,
             date == as.POSIXct(input$date))
  })

# Output the caption for the table ----------------------------------------

  output$TableOneCaption <- renderText({
    paste("Portfolio Position of ", input$trader, " on ", input$date)
  })

# Output the portfolio positions data -------------------------------------

  output$TableOne <- renderDataTable(data1(), options = list(pageLength = 5))

# Output the caption for the table ----------------------------------------

  output$TableTwoCaption <- renderText({
    paste("Exposure of ", input$trader, " on ", input$date, " by sector")
  })

# Output the exposure by sector data --------------------------------------

  output$TableTwo <- renderDataTable(data2(), options = list(pageLength = 5))

# Output the pie of exposure by sector ------------------------------------

  output$pie <- renderPlot({
    validate(need(length(data2()$exposure) >= 1, FALSE))
    pie(
      data2()$exposure,
      labels = data2()$GICSSector,
      col  = rainbow(length(data2()$exposure)),
      main = "Pie Chart of Exposure by Sector"
    )
  })
}
