#!/usr/bin/env python
# coding: utf-8

# In[ ]:


Insert in Create table to creat suspect_trades table
(
    "_id"   TEXT,
    "DateTime"  TEXT,
    "TradeId"   TEXT,
    "Trader"    TEXT,
    "Symbol"    TEXT,
    "Quantity"  INTEGER,
    "Notional"  TEXT,
    "TradeType" TEXT,
    "Ccy"   TEXT,
    "Counterparty"  TEXT,
    "cluster"   INTEGER,
    PRIMARY KEY("_id")
);"""

