from fastapi import FastAPI, HTTPException
import importlib
import configparser
import configparser
import sys
sys.path.append('..')
from modules.db.db_connection import sql_conn, mongo_conn



conf = configparser.ConfigParser()
def readconfig():
    conf.read("../config/script.config")
    x = conf.get("config","sqldatabase")
    SQLname = x
    #print(x)

    return SQLname

def readparams():
    conf.read("../config/script.params")
    x = conf.get("params","db")
    db = x
    x = conf.get("params","collection")
    collection = x
    x = conf.get("params","url")
    url = x
    return db, collection, url

month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
app = FastAPI()

@app.post("/")
async def root(DateTime: str=None, TradeId: str = None, Trader: str = None, Symbol: str= None, Quantity: int = None, Notional: float = None, TradeType: str = None, Ccy: str = None, Counterparty: str = None):
    SQLname = readconfig()
    mongo_db, mongo_collection, url = readparams()

    try:
        collection = mongo_conn(mongo_db, mongo_collection, url)
    except:
        raise HTTPException(status_code=404, detail = "Load MongoDB database error! This may be because you don't have a mongoDB dataset loacally or the local service is not started.")
    try:
        conn, c = sql_conn(SQLname)
    except:
        raise HTTPException(status_code=404, detail = "Load sqlite3 database error! This may be because you don't provide a valid path to the sql database. You should provide your path like '/root/.../Equity.db'.")

    if DateTime is None:
        raise HTTPException(status_code=403, detail = "Missing information: DateTime")
    if Trader is None:
        raise HTTPException(status_code=403, detail = "Missing information: Trader")
    if Symbol is None:
        raise HTTPException(status_code=403, detail = "Missing information: Symbol")
    if Quantity is None:
        raise HTTPException(status_code=403, detail = "Missing information: Quantity")
    if Notional is None:
        raise HTTPException(status_code=403, detail = "Missing information: Notional")
    if TradeType is None:
        raise HTTPException(status_code=403, detail = "Missing information: TradeType")
    if Ccy is None:
        raise HTTPException(status_code=403, detail = "Missing information: Ccy")
    if Counterparty is None:
        raise HTTPException(status_code=403, detail = "Missing information: Counterparty")

    mongo_date = DateTime.split("(")[1].split("T")[0]
    date = mongo_date.split("-")
    date = date[2] + "-" + month[int(date[1])-1] + "-" + date[0]


    # We will perform two checks: 1. ensure price (Notional/Quantity) is between low and high; 2. ensure the sum of Quantity is less then volume

    equity_prices = []
    sql_query="SELECT high, low, volume FROM equity_prices WHERE cob_date == '"+date+"'and symbol_id ==" + "'"+Symbol+"'"
    cursor = c.execute(sql_query)
    for x in cursor:
        equity_prices.append(x)
    if equity_prices == []:
        raise HTTPException(status_code=402, detail = "Incorrect date or symbol")
    high = equity_prices[0][0]
    low = equity_prices[0][1]
    volume = equity_prices[0][2]

    # Check price at first
    price = Notional/Quantity
    if price < low or price > high:
        raise HTTPException(status_code=403, detail = "Price is not correct"+sql_query)

    # Check amount
    mongo_query = {"DateTime":{ "$regex":mongo_date,"$options":"$i"}, "Symbol":Symbol}
    trader_reco = []
    for x in collection.find(mongo_query):
        trader_reco.append(x)
    total_amount = Notional
    for i in trader_reco:
        total_amount += i['Notional']
    if total_amount > volume:
        raise HTTPException(status_code=403, detail = "Amount is not correct")
    insertion = { "DateTime": DateTime, "TradeId":TradeId, "Trader": Trader, "Symbol": Symbol, "Quantity": Quantity, "Notional": Notional, "TradeType": Notional, "Ccy": Notional, "Counterparty": Counterparty}
    x = collection.insert_one(insertion)
    return "Trade submitted successfully"