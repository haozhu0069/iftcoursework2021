import datetime
import re
import sys
import os

from modules.db import db_connection
from modules.db.SQL import CreateTable

def create_table():
    CreateTable.create_table()

def process_data(date_str):
    # create table structure
    create_table()
    #Get mongodb connection
    collections = db_connection.get_mongo_connection()
    # Query data for 2020-01-06
    first_query = {'DateTime': re.compile(date_str)}
    result = []
    positions={}
    # Connecting to Equity.db
    conn = db_connection.get_sqlite_connection()
    cur = conn.cursor()
    suspectable_counts=0
    for item in collections.find(first_query):
        flag = True
        # Calibration data is qualified
        # Calibration price range
        notional = item["Notional"]
        quantity = item["Quantity"]
        price = notional / quantity
        symbol = item["Symbol"]
        ccy = item["Ccy"]
        trader = item["Trader"]
        date = datetime.datetime.strptime(date_str, "%Y-%m-%d")
        date_str1=datetime.datetime.strftime(date,'%Y-%b-%d')
        date_str2=datetime.datetime.strftime(date,'%Y%m%d')
        pos_group = trader+date_str2+symbol+ccy
        tmp={}
        tmp["pos_id"]=trader+date_str2+symbol
        tmp["cob_date"] = date_str1
        tmp["trader"] = trader
        tmp["symbol"] = symbol
        tmp["ccy"] = ccy
        if pos_group not in positions.keys():
            tmp["net_quantity"]=quantity
            tmp["net_amount"]=notional
            positions[pos_group]=tmp
        else:
            positions[pos_group]["net_quantity"]+=quantity
            positions[pos_group]["net_amount"]+=notional
        cur.execute("SELECT * FROM equity_prices where symbol_id='{}' and cob_date='{}'".format(symbol, date_str1))
        res = cur.fetchone()
        if res:
            high = res[2]
            low = res[3]
            if price >= low or price <= high:
                flag = False
        if flag:
            # Suspicious data
            print("find out a suspectable trade:")
            print(item)
            item["date"]=date_str1
            result.append(item)
            suspectable_counts+=1
    print("The system found a total of suspectable trades:{}".format(suspectable_counts))
    print("Now,insert them into the table trades_suspects")
    if len(result)> 0:
        sql="""INSERT INTO trades_suspects(trade_id, date, trader, symbol, quantity, notional, trad_type, ccy, counterparty) 
VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}');"""
        for row in result:
            trade_id=row["TradeId"]
            trade_type=row["TradeType"]
            notional = row["Notional"]
            quantity = row["Quantity"]
            symbol = row["Symbol"]
            ccy = row["Ccy"]
            trader = row["Trader"]
            counterparty = row["Counterparty"]
            date = row["date"]
            try:
                cur.execute(sql.format(trade_id,date,trader,symbol,quantity,notional,trade_type,ccy,counterparty))
                print("Successfully inserted data with trade id:{}".format(trade_id))
            except Exception as e:
                print("Data with id:{} already exists".format(trade_id))
                print("Failed to inserted data with trade id:{}".format(trade_id))
                continue
            conn.commit()
    print("Aggregate Quantity and Notional for all trades by date, trader, symbol, ccy")
    for key in positions.keys():
        tmp=positions[key]
        sql = """INSERT INTO portfolio_positions(pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) 
VALUES ('{}','{}', '{}','{}','{}', '{}', '{}');"""
        pos_id=tmp["pos_id"]
        cob_date=tmp["cob_date"]
        trader=tmp["trader"]
        symbol=tmp["symbol"]
        ccy=tmp["ccy"]
        net_quantity=tmp["net_quantity"]
        net_amount=tmp["net_amount"]
        cur.execute("select * from portfolio_positions where pos_id='{}'".format(pos_id))
        tup=cur.fetchone()
        if tup:
            print("Data with id:{} already exists".format(pos_id))
        else:
            cur.execute(sql.format(pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount))
            conn.commit()
            print("Successfully inserted data with pos_id:{}".format(pos_id))
    cur.close()
    conn.close()


if __name__ == '__main__':
    if len(sys.argv)>0:
        for d in sys.argv:
            # try:
                print("handle data in {}".format(d))
                process_data(d)
                print("processing is completed, query the related datas in the database")
            # except  as e:
            #     print("Parameter format error, please enter date strings in format:YYYY-mm-dd")






