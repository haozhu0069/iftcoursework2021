# Import packages
library(RSQLite)
library(mongolite)
library(dplyr)
library(lubridate)
library(tidyr)
library(tibble)
library(DT)
library(shiny)



# set up directories
GITRepoDirectory <- "/Users/abrazar/iftcoursework2021" 

# Connecting to SQLite
conSql <- dbConnect(dbDriver("SQLite"),dbname = "/Users/abrazar/iftcoursework2021/000.DataBases/SQL/Equity.db")

# build pipeline and Import portfolio position and equity static from SQL
SQLQueryPortfolio1 <- paste0("SELECT * FROM portfolio_positions LEFT JOIN equity_static ON portfolio_positions.symbol=equity_static.symbol ORDER BY 'cob_date' ;")
PortfolioInfo <- as.data.frame(dbGetQuery(conSql, SQLQueryPortfolio1))
SQLQueryPortfolio2 <- paste0("SELECT * FROM portfolio_positions LEFT JOIN equity_prices ON portfolio_positions.symbol=equity_prices.symbol_id where portfolio_positions.cob_date=equity_prices.cob_date ORDER BY 'cob_date' ;")
AdditionalInfo <- as.data.frame(dbGetQuery(conSql, SQLQueryPortfolio2))
PortfolioInfo <- PortfolioInfo[, -8]

#replace the net_amount by net_quantity*close
PortfolioInfo <- PortfolioInfo[, -7]
PortfolioInfo$net_amount <- PortfolioInfo$net_quantity*AdditionalInfo$close

#calculate the total amount of each trader in each day
DailySum <- aggregate(x = PortfolioInfo$net_amount, by = list(PortfolioInfo$trader, PortfolioInfo$cob_date), sum)
#calculate the amount of every sector of each trader in each day
DailySectorSum <- aggregate(x = PortfolioInfo$net_amount, by = list(PortfolioInfo$trader, PortfolioInfo$cob_date, PortfolioInfo$GICSSector), sum)

colnames(DailySum) <- c("Trader", "cob_date", "TotalAmount")
colnames(DailySectorSum) <- c("Trader", "cob_date", "GICSSector", "TotalSectorAmount")

#calculate the exposures
DailySectorSum$exposures <- NA
for (i in 1:nrow(DailySectorSum)) {
  for (j in 1:nrow(DailySum)) {
    if ((DailySectorSum$Trader[i] == DailySum$Trader[j]) && (DailySectorSum$cob_date[i] == DailySum$cob_date[j])) {
      DailySectorSum[i, "exposures"] <- DailySectorSum$TotalSectorAmount[i] / DailySum$TotalAmount[j]
    }
  }
}

PortfolioInfo$cob_date <- dmy(PortfolioInfo$cob_date)
DailySectorSum$cob_date <- dmy(DailySectorSum$cob_date)



# Define UI for dataset viewer app ----
shinyUI(fluidPage(

  # App title ----
  titlePanel("Portfolio Position"),

  # Sidebar layout with a input and output definitions ----
  sidebarLayout(

    # Sidebar panel for inputs ----
    sidebarPanel(


      # choose trader
      selectInput(
        inputId = "trader", label = "Trader",
        choices = unique(PortfolioInfo$trader),
        selected = "DGR1983"
      ),
      # Select date
      dateInput("date", strong("Date"),
        value = "2020-01-03",
        min = "2020-01-03", max = "2021-11-10"
      ),
    ),
    # Main panel for displaying outputs ----
    mainPanel(
      DTOutput("PortfolioInfodt"),
      DTOutput("DailySectorSumdt")
    )
  )
))




