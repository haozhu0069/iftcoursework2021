//1.  Aggregate all Sectors and return the average Beta by Sector
db.CourseworkOne.aggregate([
	{$match: {} },
	{$group: {_id: "$StaticData.GICSSector", average: {$avg: "$MarketData.Beta"} } }])
	
//2. Aggregate all subindustries in IT sector and return the average PEratio
db.CourseworkOne.aggregate([
	{$match: {"StaticData.GICSSector": "Information Technology"} },
	{$group: {_id: "$StaticData.GICSSubIndustry", pratio: {$avg: "$FinancialRatios.PERatio"} } }])

//3. Filtering fields on the 7 positions made by trader
db.CourseworkOne.find( { $and: [ { Symbol: { $in: ['AAPL','ACN', 'AMAT', 'AVGO', 'IBM', 'NVDA', 'ORCL'] }, "FinancialRatios.PERatio": {"$gte": 15} } ] } ).pretty()
