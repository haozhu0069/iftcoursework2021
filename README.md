# UCL - IFT Big Data Quantitative Finance Coursework
- [UCL - IFT Big Data Quantitative Finance Coursework](#ucl---ift-big-data-quantitative-finance-coursework)
  - [Coursework Three](#coursework-three)
    - [Starting point](#starting-point)
    - [Objectives](#objectives)
    - [Use Cases](#use-cases)
      - [Use Case One (Python and R)](#use-case-one-python-and-r)
      - [Use Case Two (Python and R)](#use-case-two-python-and-r)
      - [Use Case Three.a (R)](#use-case-threea-r)
      - [Use Case Three.b (Python)](#use-case-threeb-python)
    - [Database](#database)
    - [Code Submission](#code-submission)
    - [Report](#report)
  - [Coursework Two](#coursework-two)
    - [Objectives](#objectives-1)
      - [Use case: incorrect trade detection](#use-case-incorrect-trade-detection)
      - [Use case: risk policy breach](#use-case-risk-policy-breach)
      - [Use case: benchmark trader performance](#use-case-benchmark-trader-performance)
    - [Database description](#database-description)
    - [Code Submission](#code-submission-1)
    - [Report](#report-1)
  - [Coursework One](#coursework-one)
    - [Objectives](#objectives-2)
    - [Sql and NoSql database for coursework one](#sql-and-nosql-database-for-coursework-one)
    - [Requirements](#requirements)
      - [Code Submission](#code-submission-2)
      - [Report](#report-2)

## Coursework Three
[Published 16/01/2022 09:30:01]

### Starting point
Before commencing any work on this coursework please make sure:
1. Best practices on Moodle wiki are read thoroughly and understood;
2. Read the entire Coursework Three Section from here to the end at *least two times*. The section contains all details for structuring, developing and submitting the code. Requirements and guideline described herein must be followed in order to successfully complete the coursework.
3. Please make sure that any developments refer to the databases stored in Mongo or under 000.DataBase. No copy of databases into personal folders are allowed.
4. Structure your project in a rational and clean way as suggested in best practices.
5. Use relative references path and do not hardcode directories in your script that refers to your personal file system.


### Objectives
The objective of Coursework Three is to provide analytics solutions based on MongoDB database and SQLite database. Python and R are equally acceptable for this submission. In order to add more colour to the reporting analytics, the students can also leverage on JavaScript, HTML and css. One *Use Case* should be selected among the three business cases provided in next section.


### Use Cases

#### Use Case One (Python and R)
Front office is asking to provide with an analytics report with the details of main allocations and historical trends for a given trader. In order to achieve this objective, the ask is to select one trader and generate a report for End of day 2021-11-12.

The following steps are required:
1. Create End of Day portfolio allocation for the selected trader (Mongodb Database Equity Collection CourseworkTwo)
2. Identify Main Portfolio Metrics in terms of Risk and Performance of the trader portfolio
3. Create a html report that contains table and charts that can be useful to monitor the trading activity of the selected trader.

R and Python can be used to generate the final report. If R is selected, the reporting script should leverage on R and RMarkdown while for Python pandas and jinja2 are recommended.

#### Use Case Two (Python and R)
Second Line Risk has requested a report to monitor positions according to the Policy Risk limits established within the company. 

In order to achieve this objective, the ask is to select one trader and generate a report for End of day 2021-11-12.

The following steps are required:
1. Create End of Day portfolio allocation for the selected trader (Mongodb Database Equity Collection CourseworkTwo)
2. Identify Main Portfolio Metrics in terms of Risk and Performance of the trader portfolio
3. Create a html report that contains table and charts that can be useful to monitor the trading activity of the selected trader.

In order to achieve these objective, R and Python can be used. If R is selected, the reporting script should leverage on R and RMarkdown while for Python pandas and jinja2 are recommended.


#### Use Case Three.a (R)
Front Office trading Desk has requested a dashboard to visualise daily positions by trader. The dashboard allows users to select a date and a trader in order to display the main portfolio positions and exposures by sector.

In order to achieve these objectives:
1. Create a shiny single page application made of ui.R and server.R
2. The ui allows user to select a given date and trader in order to visualise positions and main exposures.
  
In order to achieve these objective, R and RShiny should be used.



#### Use Case Three.b (Python)
Our Business Management team has decide to provide traders the capability to programmatically trade stocks. In order to achieve this objective, the ask is create and API with a single end point where traders can submit their trades.

In order to achieve these objectives:
1. create a single end point api where traders can submit trades.
2. the trades submitted to the API will be then written into MongoDB Database Equity Collection CourseworkTwo;
3. Before the transaction can be accepted, a sanity check on trades amount and trades prices must be performed in order to ensure that trades do not contain wrong information.

In order to achieve this objective, FastAPI framework should be used.

### Database
**MongoDB**
Database: Equity 
Collection: CourseworkOne
Collection: CourseworkTwo
**SQL**
Equity.db

Please note, no copy of the database are allowed in personal folders. All database connection MUST refer to folder ./000.Databases or to MongoDB collections.


### Code Submission
Code must be submitted in bitbucket. The repository for submission is [iftcoursework2021](https://bitbucket.org/uceslc0/iftcoursework2021/src/master/). Students upload Coursework Number 2 code in their own folders created in Coursework One.

The folder structure is as per following (with * replaced by py or R extension)

```
NameSurname
    |-1.CourseworkOne/
    |--1.SQLQueries.txt
    |--2.NoSQLQueries.js
    |--.gitkeep
    |-2.CourseworkTwo/
    |--config/
    |---script.config
    |---script.params
    |--modules/
    |--static/
    |--test/
    |--Main.*
    |--.gitkeep
    |_3.CourseworkThree
    |--config/
    |---script.config (.ini files are good too)
    |---script.params
    |--modules/
    |--static/
    |--src/
    |---App.* (if Shiny is used, then here should be placed server.R and ui.R)
    |--test/
    |--Home.html (mandatory for Use case 1 and 2, optional for the other)
```

Not all sub-folders must be used. If a given folder is empty, please make sure a .gitkeep is placed in the directory so that it will be retained in the commit.

Subfolder ./modules can be further structured in sub-folders name after what the contain. an example could be:

```
|-modules/
|--db/
|---db_connection.*
|--input/
|---input_loader.*
|--output/
|---script_purposes.*
|--etc..etc..

```
All scripts should be parametrised in such a way that it is possible to trigger them by any environment.
As an example:

```
Rscript ./src/App.R param1 param2 param3 ...
Python ./src/App.py param1 param2 param3 ...

```

config sub-folder should contain all the configurations and params that can be amended without affecting the code logic.

**test** sub-folder contains a test files. While is non-mandatory part of the coursework, it is encouraged to perform some functional or unit test to evaluate if modules or functions are fit for use. Any standard module/library can be used for this, it is suggested to look into pytest or unittest (python) and testthat (R).

_Please note, files in test folder are used exclusively for testing component or units only. Do not use in the main App.py or App.R. The purpose of this folder is to serve for test cases only!_

### Report
A final report of maximum 7,500 word must be submitted to Turnitin. The report should be structured in such a way that a summary of the all courseworks is provided.

Accordingly, the structure should be:
1. Introduction;
2. Background and Methodology;
3. Databases (description of databases both SQL and NoSQL + schema description)
4. Modelling of business case, challenges and description;
5. Conclusions with a summary of main topic covered over the CourseWork One, CourseworkTwo and Coursework Three.


## Coursework Two

[Published 05/12/2021 00:00:01]

### Objectives

The objective of Coursework Two is to demonstrate a practical understanding of building data pipelines in order to extract data from a database, manipulate data and then load into a database.

In order to achieve this objective, the student can use R/Python programming languages and perform the following tasks:

* extract from a database using Python/R, transform data with Python/R and load transformed data into a database.
* extract data from a database, load into another database and transform data using a set of stored database procedures.

The student should select one of the following use cases:

#### Use case: incorrect trade detection

Traders submit single trades at any point in time during the day of 2021-11-11 & 2021-11-12. Traders might make mistakes when they submit their trades, it is your responsibility to verify that trades submitted by Front Office are genuine and in line with market expectations. If a trade is deemed to be suspect, (i.e. incorrect trade price or incorrect quantity), it must be reported.

For each day, the objective of this use case is to:

* retrieve all trades as per end of day (i.e. one run for all trades on 2021-11-11 and one run for 2021-11-12) from MongoDB db Equity collection CourseworkTwo (see instructions below);
* for each day, check that trades are consistent with expectations (i.e. there is no fat fingers error, inconsistency between quantity traded and notional amounts, genuine mis-pricing on the trade); Here, you can apply any model to detect whether a trade is genuine or not. This can be done in multiple ways (trade vs other trades, trade vs price, hypothesis testing, clustering analysis), any approach is valid however please describe in the methodology section why you pick one approach over another.
* if any, create a new table in SQLite Database called **trades_suspects**. Load suspect trades into a new table in SQL. The creation statement for this table should be stored in "./modules/db/SQL/CreateTable.*" where * should be replaced by .py or .R. Table should be designed to follow best practices and must then include a primary key and a foreign key.
* aggregate Quantity and Notional for all trades by date, trader, symbol, ccy and insert the results into portfolio_positions.(see section on Database description)


#### Use case: risk policy breach

Second Line Risk has agreed with us (Front Office) that we will be monitoring all risk positions for our traders. In particular, for 2021-11-11 & 2021-11-12 end of day, we will need to:

* retrieve all trades as per end of day from MongoDB db Equity collection CourseworkTwo (see instructions below);
* aggregate all trades by Trader, ISIN, Currency and Date;
* retrieve all policy limits from SQLite database;
* check if any new aggregated position is breaching a policy limit (check for at least one of the below policy limits);
* if a policy limit is breached, design and create a new SQLite table called **policy_breaches** and load all breaches into the new table. The creation statement for this table should be stored in "./modules/db/SQL/CreateTable.*" where * should be replaced by .py or .R. Table should be designed to follow best practices and must then include a primary key and a foreign key.
* load all trades into portfolio_positions (see section on Database description)


*Policy limits description:*

* if limit_end is not null, limit has been changed or decommissioned;
* long/short consideration: max amount in USD$ (mark to market) for a single stock position;
* volume relative (%): max position given daily volumes. The trader cannot have more than X% of daily volume as ratio of position consideration. position must be mark to market over the volume;
* sector relative: max concentration of portfolio allocations by sector. all stocks in a portfolio for a sector cannot exceed the X% specified. this is the result of sum portfolio value as quantity times market price (mark to market positions) by sector over the total mark to market portfolio value.
* ES relative (%): the trader cannot run a portfolio that has an expected shortfall greater than X%. Expected shortfall is calculated as the average of 5 biggest losses of the last 50Days portfolio returns with an holding period of 3 days (i.e. Price today / Price 3 Days ago - 1)
* Volatility (%): the trader cannot run a portfolio that has a portfolio annualised return volatility greater than the limit express in percentages. Look-back for volatility calculations is 50Days.
* VaR Relative (%): the trader cannot run a portfolio that has a Value at Risk greater than X%. Value at Risk is calculated on the fully revaluated portfolios returns, look-back period 50Days, holding period 3 days, 99% confidence level. Value at Risk can be estimated by using either historical simulations or the Variance-Covariance Method. If the Variance-Covariance Method is used, all parameters described above should be used to estimate the portfolio variance over 50Days. The assumption of normal distributions of returns is accepted.


#### Use case: benchmark trader performance

As front office analyst, we would like to verify if one of our traders strategy is outperforming a given benchmark. In order to achieve this goal:

* analysis date 2021-11-10.
* identify one fund/trader to monitor and check which benchmark could be the best to verify the performance.
* as starting point, construct a benchmark by using the MarketCap field provided in Coursework one. In order to construct a market cap weighted benchmark it is possible to follow different specification (MSCI, FTSE, Blackrock or Axioma). It is further possible to tilt the benchmark towards a particular factor that you think fits best the trader. This can be achieved by tilting on industry/sector, on market conditions (volatility, momentum, liquidity) or on dividend yield / beta / cash flows. Some starting info can be found [here](https://research.ftserussell.com/products/downloads/factor-index-construction-methodology-paper.pdf)
* once a benchmark is derived, create a table and store all benchmark returns for the past year into a new sql table called benchmark_returns.
* for the selected trader/portfolio, derive the daily returns for the portfolio allocations on 2021-11-10 over the past 20 days.
* finally check, how many times the trader has exceeded the benchmark in the past 20 days prior to 2021-11-11.


### Database description
**CourseworkTwo.json**

import into MongoDB the file  ./000.DataBases/NoSQL/CourseworkTwo.json as:

```
use Equity
db.createCollection("CourseworkTwo")
db.CourseworkTwo.insert() // copy and paste the full json file between the rounded brackets.
```


Data Description:

DateTime: Mongo ISODate. gte,gt,lt,lte,eq can be used as an example {ISODate("2020-01-06T08:19:56.000+00:00")};
TradeId: Unique identifier of a trade;
Trader: Trader identifier;
Symbol: Stock identifier;
Quantity: Number of shares exchanged;
Notional: Value of shares exchanged;
TradeType: describes whether trade is a "BUY" or a sell;
Ccy: currency in which notional is denominated;
Counterparty: trading counterparty.

To achieve net positions at the end of the day, trades should be filtered for a given date and aggregated by Trader, Symbol, Ccy by summing Quantity and Notional.

**Equity.db**
Data description:

```
portfolio_positions (
    pos_id TEXT PRIMARY KEY, -- unique identifier made of traderid + cob_date (Ymd) + symbol_id
    cob_date TEXT NOT NULL,  -- date in format of DD-Mmm-YYYY
    trader TEXT NOT NULL,    -- trader identifier
    symbol TEXT NOT NULL,    -- stock identifier
    ccy TEXT NOT NULL,       -- currency for notional amount   
	net_quantity INTEGER NOT NULL,      -- aggregation of all trades made in a given date by trader & stock as sum
	net_amount INTEGER NOT NULL,        -- aggregation of all notional made in a given date by trader & stock as sum
	FOREIGN KEY (symbol) REFERENCES equity_static (symbol)
)
```

in order to insert data into SQLite, please see example from our classes or:

```

INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount),
VALUES (
    ...
)

```
NB ... should be replaced by data


### Code Submission
Code must be submitted in bitbucket. The repository for submission is [iftcoursework2021](https://bitbucket.org/uceslc0/iftcoursework2021/src/master/). Students upload Coursework Number 2 code in their own folders created in Coursework One.

The folder structure is as per following (with * replaced by py or R extension)

```
NameSurname
    |-1.CourseworkOne/
    |--1.SQLQueries.txt
    |--2.NoSQLQueries.js
    |--.gitkeep
    |-2.CourseworkTwo/
    |--config/
    |---script.config
    |---script.params
    |--modules/
    |--static/
    |--test/
    |--Main.*
    |--.gitkeep
    |_3.CourseworkThree
    |__.gitkeep
```

Not all sub-folders must be used. If a given folder is empty, please make sure a .gitkeep is placed in the directory so that it will be retained in the commit.

Subfolder ./modules can be further structured in sub-folders name after what the contain. an example could be:

```
|-modules/
|--db/
|---db_connection.*
|--input/
|---input_loader.*
|--output/
|---script_purposes.*
|--etc..etc..

```

All scripts should be parametrised in such a way that it is possible to trigger them by any environment.
As an example:

```
Rscript Main.R param1 param2 param3 ...
Python Main.py param1 param2 param3 ...

```

config sub-folder should contain all the configurations and params that can be amended without affecting the code logic.

test sub-folder contains a test files. While is non-mandatory part of the coursework, it is encouraged to perform some functional or unit test to evaluate if modules or functions are fit for use. Any standard module/library can be used for this, it is suggested to look into pytest or unittest (python) and testthat (R).


### Report

Along side to code developments, the student must write a report (1500 Words max) that will be submitted in Turnitin according to the deadlines specified at the top of this page. 

The report should be structured with:

1. Introduction;
2. Section 2. Present business case, challenges and data;
3. Section 3. Present the solution to the challenge:
   1. What are the possible approaches (literature review, model selection), 
   2. Which approach is used and why it is preferable among others, 
   3. Implementation, 
   4. Results.
4. Conclusion: conclude the work and summarise the key findings.




## Coursework One
[Published 14/11/2021 00:00:01]
### Objectives
Objective of Coursework One is to demonstrate familiarity with both SQL and NoSQL MongoDB syntax for querying data.
In addition, the student will also show that she/he has understood the Git workflow by submitting the main queries into Bitbucket.

### Sql and NoSql database for coursework one
In order to complete each coursework, the student must use the databases stored in [iftcoursework2021](https://bitbucket.org/uceslc0/iftcoursework2021/src/master/) under folder 000.DataBases. The SQL database can be accessed by using SQLite Portable Browser or any other type od SQLite User Interface and will be under the folder 000.DataBases/SQL/Equity. 

The NoSQL data, provided in a json file (CourseworkOne.json), requires loading into MongoDB. In order to load the json file on MongoDB, please follow these steps:
1. Open MongoDb Shell;
2. In MongoDB shell perform the following actions:

```
use Equity
db.createCollection("CourseworkOne")
db.CourseworkOne.insert() // copy and paste the full json file between the rounded brackets.
```

### Requirements
The student should develop at least two queries for each SQL and NoSQL database. The complexity and sophistication of the query is left at student discretion.


#### Code Submission
For each coursework submission code must be submitted in bitbucket. The repository for submission is (iftcoursework2021)[https://bitbucket.org/uceslc0/iftcoursework2021/src/master/].

In order to submit the coursework, the student needs to follow the following steps:
1. Clone the repository;
2. create a new branch;
3. add your own developments in a dedicated folder;
4. the dedicated folder has to be placed under "./iftcoursework2021/" and is structured as following:

```
NameSurname
    |_1.CourseworkOne
    |__.gitkeep
    |_2.CourseworkTwo
    |__.gitkeep
    |_3.CourseworkThree
    |__.gitkeep
```
*Please see example under this directory for folder name called "LucaCocconcelli"*

Two files for Coursework One submission will then be placed under 1.CourseworkOne with following naming conventions:
* "1.SQLQueries.txt"
* "2.NoSQLQueries.js"

Once this is completed, in order to submit the code to Bitbucket:
1. check git status;
2. if the local repository is behind the master, please perform a git pull;
3. add & commit;
4. push the origin to the branch;
5. go to bitbucket and create a pull request by merging your branch into the master.

Please note, the iftcoursework2021 will go back to read only mode at the End of Day for Coursework One submission (23:59 GMT). After this, if you'd like to submit your code to bitbucket, please send an email containing the folder (zipped) to l.cocconcelli@ucl.ac.uk

#### Report
Along side to code developments, the student must write a report (500 Words max) that will be submitted in Turnitin according to the deadlines specified at the top of this page. The report should be structured with:
1. Short Introduction;
2. Brief description of SQL vs NoSQL database;
3. Query documentation: for each query provided, please list: 
   1. background (why I need this query), 
   2. aims (what I will achieve with my query), 
   3. approach (how I addressed the challenge and what each statement does), 
   4. output (describe main results obtained).
4. Conclusion: conclude the work and summary of key findings.
