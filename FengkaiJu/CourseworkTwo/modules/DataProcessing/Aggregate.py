import pymongo
import sqlite3

def aggregateData(database,date):
    match={"DateTime":{"$regex":"ISODate\("+date}}
    group={}
    group['_id']= {"trader":"$Trader","symbol":"$Symbol","ccy":"$Ccy"}
    group['net_notional']={"$sum" : "$Notional"}
    group['net_quantity']={"$sum" : "$Quantity"}

    pipeline=[{'$match':match},{'$group' : group}]
    aggregate_data=database.aggregate(pipeline)

    return aggregate_data
