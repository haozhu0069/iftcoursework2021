#Using K-means Clustering for 'Quantity' and 'Notional'
km = KMeans(n_clusters=2)
trade_predicted = km.fit_predict(trades_date2_df3[['Notional','Quantity']])

trades_date2_df3['cluster']=trade_predicted
trades_date2_df3.head()


trades_date2_df1 = trades_date2_df3[trades_date2_df3.cluster==0]

trades_date2_df2 = trades_date2_df3[trades_date2_df3.cluster==1]

trades_date2_df = trades_date2_df3.applymap(str)
display (trades_date2_df1)


# In[30]:


#The outlier in this cluster
display(trades_date2_df2)


# In[32]:


km = KMeans(n_clusters=2)
trade_predicted = km.fit_predict(trades_date1_df[['Notional','Quantity']])

trades_date1_df['cluster']=trade_predicted
trades_date1_df.head()


trades_date1_df1 = trades_date1_df[trades_date1_df.cluster==0]

trades_date1_df2 = trades_date1_df[trades_date1_df.cluster==1]
display (trades_date1_df1)


# In[33]:


#The outlier in another cluster
display(trades_date1_df2)


# In[ ]:


display(trades_date1_df2)
display(trades_date2_df2)
