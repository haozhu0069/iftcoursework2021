/*
use Equity
db.createCollection("CourseworkOne")
db.CourseworkOne.insert() // copy and paste the full json file between the rounded brackets.
*/


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Query 1 for NoSQL
*/--------------------------------------------------------------------------------------------------------------------------------------------------------------------

db.CourseworkOne.stats()
db.CourseworkOne.find().pretty().count()
db.CourseworkOne.find().limit(5)
db.CourseworkOne.distinct("StaticData.GICSSector")
db.CourseworkOne.find({"StaticData.GICSSector":'Health Care'}).pretty().count()


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Query 2 for NoSQL
*/--------------------------------------------------------------------------------------------------------------------------------------------------------------------

db.CourseworkOne.find().sort({"FinancialRatios.PERatio":-1}).limit(1)  
db.CourseworkOne.find().sort({"FinancialRatios.PERatio":+1}).limit(1)

/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
