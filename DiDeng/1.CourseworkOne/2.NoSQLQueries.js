use equity // create the database
db.createCollection("CourseworkOne")   // create the collection
db.CourseworkOne.insert()   // insert the full json file

QUERY 1:
db.CourseworkOne.aggregate([
	{$match: {GICSSector: "Financials"} },
	{$group: {_id: "$GICSSubIndustry", average: {$avg: "$Beta"} } }])

QUERY 2:
db.CourseworkOne.find({PERatio: {"$gte": 30}, GICSSector: {"$eq": "Financials" }}) 



