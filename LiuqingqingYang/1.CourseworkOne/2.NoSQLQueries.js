//NoSQL Query 1
db.CourseworkOne.find(
    {$and:[{"FinancialRatios.PERatio":{"$lte":25}},{"StaticData.GICSSector":{"$in":["Financials", "Information Technology"]}}]},
    {"StaticData.SECfilings":0, "StaticData.GICSSubIndustry":0}).sort({"FinancialRatios.DividendYield":-1})

//NoSQL Query 2
db.CourseworkOne.aggregate([
	{$match: {"StaticData.GICSSector":{"$in":["Financials", "Information Technology", "Energy"]}}},
	{$group: {_id:"$StaticData.GICSSector", num_GICSSector:{$sum:1}, total_MarketCap:{$sum:"$MarketData.MarketCap"}, avg_Price:{$avg:"$MarketData.Price"}}},
    {$match: {$or:[{total_MarketCap:{"$gt":2000000}}, {avg_Price:{"$lt":50}}]}}])

