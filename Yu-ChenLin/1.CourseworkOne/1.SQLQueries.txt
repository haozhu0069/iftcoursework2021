--query1
SELECT equity_prices.price_id,
CASE WHEN equity_prices.close> equity_prices.open THEN 'up'
WHEN equity_prices.close< equity_prices.open THEN 'down'
ELSE 'flat'
END AS Trend,
equity_prices.high - equity_prices.low AS PriceDifference,
volume,
symbol_id,
equity_static.security,
equity_static.GICSSector
FROM equity_prices
LEFT JOIN equity_static ON equity_prices.symbol_id = equity_static.symbol
ORDER BY PriceDifference DESC
LIMIT 100;

--query2
SELECT net_quantity*close - net_amount AS pnl,
symbol_id,
net_quantity,
close,
trader_id,
fund_name,
portfolio_positions.cob_date
FROM portfolio_positions
INNER JOIN equity_prices ON portfolio_positions.symbol = equity_prices.symbol_id
INNER JOIN trader_static ON portfolio_positions.trader = trader_static.trader_id
GROUP BY pnl
HAVING equity_prices.cob_date = '03-Jan-2020';


