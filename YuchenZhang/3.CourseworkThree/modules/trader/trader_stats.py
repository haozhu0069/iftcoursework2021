import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy.stats import norm
import pandas as pd
import numpy as np


def generate_trader_plot(tradeRecord, equityStatic, trader):
    fig = plt.figure(constrained_layout=True, figsize=(12, 12))
    gs = fig.add_gridspec(2, 1)
    ax1 = fig.add_subplot(gs[0])
    ax2 = fig.add_subplot(gs[1])


    # Trade Record for a given trader
    trader_record = tradeRecord[tradeRecord.Trader == trader]

    # ----------- 1. Plot investment trend ------------------------
    # Aggregate trader_record by date and calculate the sum of Quantity and Notional
    trend = trader_record.groupby('Date').agg(
        {'Notional': 'sum', 'Quantity': 'sum', 'Symbol': ['unique', 'nunique']})
    trend.reset_index(inplace=True)
    # Rename the columns and get trend dataframe for a given trader
    trend.columns = trend.columns.map(
        lambda x: x[0] + '_'+x[1] if (x[1] != '') else x[0])

    # Plot daily Notional in the past trading days
    ax1.plot(trend['Date'], trend['Notional_sum'],
             '-o', color='#914AB0', linewidth=2)
    ax1.set(xlabel='Date', ylabel='Notional')
    ax1.set_title(f'Daily Net Notional for Trader {trader}')
    ax1.xaxis.set_tick_params(labelsize=10)
    ax1.xaxis.set_major_locator(mpl.dates.DayLocator(interval=5))
    ax1.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    ax1.grid(True)

    # ----------- 2. Plot allocations  -----------------------------------
    # Aggregate trader_record by Symbol and calculate Notional for individual equity and sort it in descending order
    trader_record_12 = trader_record[trader_record['Date'] == '2021-11-12']
    allocations = trader_record_12.groupby('Symbol', as_index=False)[
        ['Notional']].sum().sort_values(by='Notional', ascending=False)

    # Plot the top 20 largest allocations in trader's portfolio
    ax2.bar(allocations['Symbol'][:20],
            allocations['Notional'][:20], color='#5f5f5f')
    ax2.set_title(f'Top 20 allocations for Trader {trader} on 2021-11-12')
    ax2.xaxis.set_tick_params(labelsize=10)
    ax2.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))

    # Save fig
    plt.savefig(f'static/images/{trader}.png', transparent=True)


def calcualte_portfolio_var(equityPrices, tradeRecord, trader):
    # Get trader's trade data
    trader_record = tradeRecord[tradeRecord.Trader == trader]
    # Get trader's 2021-11-12 trade data
    trader_record_12 = trader_record[trader_record['Date'] == '2021-11-12']
    # Calculate the trader's allocations
    allocations = trader_record_12.groupby('Symbol', as_index=False)[
        ['Notional']].sum().sort_values(by='Notional', ascending=False)
    # Calculate allocation weights
    allocations['Weight'] = allocations['Notional'] / \
        sum(allocations['Notional'])

    # Extract securities in the trader's portfolio
    trader_equity_prices = equityPrices[equityPrices.symbol_id.isin(
        allocations['Symbol'])][['close', 'cob_date', 'symbol_id']]
    # Reform the dataframe
    trader_equity_prices_pivot = trader_equity_prices.pivot(
        index='cob_date', columns='symbol_id', values='close').reset_index()
    trader_equity_prices_pivot.set_index('cob_date', inplace=True)
    # Calculate security returns
    trader_returns = trader_equity_prices_pivot.pct_change()

    # calcualte each security's historical average return
    avg_returns = trader_returns.mean().to_frame(name='avg_returns').reset_index()
    # Merge dataframes to match each security's average return and its weight in the portfolio
    allocations_merge = pd.merge(
        allocations, avg_returns, how='right', left_on='Symbol', right_on='symbol_id').set_index('symbol_id')

    # Ensure the covariance matrix has the same index sequence as the allocation_merge
    assert all(allocations_merge.index == trader_returns.cov().index)
    # calculate portfolio return
    port_return = np.dot(
        allocations_merge['Weight'].values, allocations_merge['avg_returns'].values)

    weights = np.array(allocations_merge['Weight'])
    # Calculate portfolio standard deviation
    port_stdev = np.sqrt(weights.T.dot(trader_returns.cov()).dot(weights))

    # For 1 USD investoment how much will lose at most
    value_at_risk = round((1 - norm.ppf(0.05, (1+port_return), port_stdev)), 6)
    # The trader's Notional at 2021-11-12
    notional = round(sum(trader_record_12['Notional']))
    # The trader's losses
    loss = round(notional * value_at_risk)
    # Trader's risk profile
    table = pd.DataFrame({'Value at Risk for 1 USD': [value_at_risk],
                          "Notional on 2021-11-12": [notional],
                          "VaR": [loss]})
    # Create a table plot
    fig, ax = plt.subplots(figsize=(10, 2))
    ax.axis('off')
    mpl_table = ax.table(cellText=table.values, bbox=[
        0, 0, 1, 1], colLabels=table.columns)

    for k, cell in mpl_table._cells.items():
        cell.set_edgecolor('w')
        if k[0] == 0:
            cell.set_text_props(weight='bold', color='w')
            cell.set_facecolor('#40466e')
        else:
            cell.set_alpha(0.3)

    plt.savefig(f"static/images/{trader}-VAR.png", transparent=True)
