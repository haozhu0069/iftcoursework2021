
from pymongo import MongoClient
import pandas as pd
from datetime import datetime


def get_mongo_trade_record(connection_string):
    client = MongoClient(connection_string)
    db = client['Equity']
    col = db['CourseworkTwo']

    # MongoDB query
    tradeRecord = col.find({})
    tradeRecord = pd.DataFrame(tradeRecord)

    tradeRecord['DateTime'] = tradeRecord['DateTime'].map(
        lambda x: x.split('(')[1].split(')')[0])
    tradeRecord['DateTime'] = pd.to_datetime(tradeRecord['DateTime'])

    # Create a simple date format
    tradeRecord['Date'] = tradeRecord.DateTime.map(
        lambda x: x.strftime('%Y-%m-%d'))
    tradeRecord['Date'] = pd.to_datetime(tradeRecord['Date'])
    # Remove remote dates
    tradeRecord = tradeRecord[tradeRecord.DateTime.dt.year == 2021]
    client.close()

    return tradeRecord
