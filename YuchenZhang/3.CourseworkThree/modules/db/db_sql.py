from datetime import datetime
from sqlalchemy import create_engine
import pandas as pd


def get_sql_equity_static(sqlbd_path):
    # Create engine
    engine = create_engine(
        f"sqlite:///{sqlbd_path}/000.DataBases/SQL/Equity.db")
    con = engine.connect()
    # Extract target table and put it into pandas dataframe
    equityStatic = pd.read_sql_query('SELECT * FROM equity_static', engine)
    # Close database
    con.close()

    return equityStatic


def get_sql_equity_prices(sqlbd_path):
    # Create engine
    engine = create_engine(
        f"sqlite:///{sqlbd_path}/000.DataBases/SQL/Equity.db")
    con = engine.connect()
    # Extract target table and put it into pandas dataframe
    equityPrices = pd.read_sql_query('SELECT * FROM equity_prices', engine)
    # Parse the date
    equityPrices['cob_date'] = pd.to_datetime(equityPrices['cob_date'])
    # Close database
    con.close()

    return equityPrices
