
1. -- Creating a new view and selecting data from database and combine them together with conditions:

CREATE VIEW Agilent_Technologies_Inc AS
SELECT 
  equity_static.*, 
  equity_prices.close,
  equity_prices.cob_date,
  round ((close - open)/open *100,2) AS daily_changes FROM equity_prices
INNER JOIN equity_static on equity_prices.symbol_id= equity_static.symbol
WHERE equity_prices.symbol_id ='A' AND equity_prices.cob_date LIKE '%2021';

--

2. --Checking how many days the daily changes are above average:

SELECT * FROM Agilent_Technologies_Inc
WHERE daily_changes  > (
    SELECT AVG(daily_changes)
	FROM Agilent_Technologies_Inc)
ORDER BY daily_changes DESC;
    
--

