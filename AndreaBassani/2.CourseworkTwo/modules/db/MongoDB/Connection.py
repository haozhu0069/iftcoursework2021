from pymongo import MongoClient
import config.params as cp

def MongoDB_connection():
    """connection_path:'mongodb://localhost'
       database_name:'Equity'
       collection_name:'CourseworkTwo' """

    con = MongoClient(cp.connection_path)
    db = con[cp.database_name]
    col = db[cp.collection_name]
    return col 