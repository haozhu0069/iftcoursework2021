import modules.FindTradeSuspect.SharePrice as fs
import modules.DateConvertor.Convertor as dc
import modules.db.SQL.Query as sq
import pandas as pd

def trade_and_dailyData(date):
    conv_date = dc.dateConvertor(date)
    daily_range = pd.DataFrame(sq.SQL_query (date))
    daily_range = daily_range.set_axis(['price_id','open', 'high', 'low', 'close','volume', 'currency', 'cob_date', 'symbol_id'], axis=1,inplace=False)
    share_price_trade = fs.share_price_per_trade(date)
    full_table = pd.merge(share_price_trade, daily_range, how='left', left_on='Symbol',right_on='symbol_id')
    full_table ['pos_id']  = full_table["Trader"]+conv_date[1][1]+full_table["Symbol"]
    return full_table