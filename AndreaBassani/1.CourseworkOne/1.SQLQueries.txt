—Query 1 (companies in the Information Technology and Energy sector with high liquidity for 2021)


SELECT equity_static.GICSSector,
COUNT (DISTINCT equity_prices.symbol_id) AS StockPerIndustry,
SUM(equity_prices.volume )AS VolumePerSector,
COUNT( equity_prices.symbol_id) AS  Stockstraded,
SUM(equity_prices.volume) /COUNT( equity_prices.symbol_id) AS TradePerStock
FROM equity_prices JOIN equity_static ON equity_static.symbol=equity_prices.symbol_id
WHERE equity_static.GICSSector IN ('Information Technology', 'Energy') AND equity_prices.cob_date LIKE '%2021'
GROUP BY equity_static.GICSSector
ORDER BY equity_prices.volume DESC;


— Query 2 (Find the companies with the highest daily range in the most liquid sector during 2021)

SELECT equity_static.security,
equity_static.GICSSector,
AVG(equity_prices.high-equity_prices.low) AS ChangeInPrice
FROM equity_prices JOIN equity_static ON equity_static.symbol=equity_prices.symbol_id
WHERE equity_static.GICSSector IN (
					SELECT equity_static.GICSSector
					FROM equity_prices JOIN equity_static ON equity_static.symbol=equity_prices.symbol_id
					WHERE equity_prices.cob_date LIKE '%2021'
					GROUP BY equity_static.GICSSector
					ORDER BY SUM(equity_prices.volume ) DESC
					LIMIT 1) 
AND equity_prices.cob_date LIKE '%2021'
GROUP BY equity_static.security
ORDER BY ChangeInPrice DESC
LIMIT 5;