library(lubridate)
library(dplyr)
library(RSQLite)
library(shiny)
library(shinydashboard)
library(DT)
library(ggplot2)
library(plotly)
library(rsconnect)

# Parse ARGs --------------------------------------------------------------
# Args = c("/Users/mac/Desktop/iftcoursework2021/YinuoWang/3.CourseworkThree", "script.config", "script.params")
Args <- commandArgs(TRUE)
setwd(Args[1])
source(paste0("./config/", Args[2]))
source(paste0("./config/", Args[3]))

printInfoLog("Main.R :: Script Settings Loaded...", type = "info")

# DataBase connection ------------------------------------------------------

ConSql <- dbConnect(RSQLite::SQLite(), Config$SQLDataBase$Connection) 

portfolioPositions <- getPortfolioPositionsSQL(ConSql)

equityStatic <- getEquityStaticSQL(ConSql)

portfolioPositions <- left_join(portfolioPositions, equityStatic, by = c("symbol"="symbol"))
colnames(portfolioPositions)[1] <- 'date'
colnames(portfolioPositions)[5] <- 'quantity'
colnames(portfolioPositions)[6] <- 'amount'

aggregate = aggregate(cbind(quantity,amount)~date+trader+GICSSector+ccy, data=portfolioPositions, FUN=sum)

total = aggregate(cbind(quantity,amount)~date+trader+ccy, data=portfolioPositions, FUN=sum)
upload <- select(total, date, trader, amount)

#-- exposure by sector

totalExposure <- left_join(aggregate, upload, by = c("trader"="trader","date"="date"))

listTrader <- split(totalExposure, totalExposure$trader)

exposuresBySector <- lapply(listTrader, function(x){
  x <- arrange(x, desc(date))
  x$amount <- x$amount.x
  x$total_amount <- x$amount.y
  x$exposure <- x$amount/x$total_amount
  return(x)
})

printInfoLog("Main.R :: Get output...", type = "info")

exposuresBySector <- as.data.frame(do.call(rbind, exposuresBySector), stringsAsFactors = T)
exposuresBySector <- select(exposuresBySector, date, trader,GICSSector, ccy, quantity, amount, exposure)

portfolioPlot <- select(exposuresBySector, date, trader, GICSSector, amount,exposure)
exposuresPlot <- select(exposuresBySector, date, trader, GICSSector, exposure)
# exposuresPlot$cob_date<- as.Date(exposuresPlot[,"cob_date"], format = "yyyy-mm-dd")

# Close SQL Connections -------------------------------------------

dbDisconnect(ConSql)

printInfoLog("SQLUpdate.R :: SQL Update Completed")


# Run the app ----------------------------------------------------

shinyApp(ui = ui, server = server)


# Stop the app ----------------------------------------------------

stopApp(returnValue = invisible())

