#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Weihao Sheng
# Lecture : 2021-12-19
# Topic   : Helper Functions
#--------------------------------------------------------------------------------------

#-- Print info log
#-- function for logging script info into console

printInfoLog <- function(message, type = "info"){

    type <- toupper(type)

    message <- paste0("[", type,"] ", Sys.time(), " ----- ", message, "\n \n")

    return(cat(message))
}