#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Weihao Sheng
# Coursework Two : 2021-12-19
# Topic   : Incorrect Trade Detection
#--------------------------------------------------------------------------------------

# Import Libraries --------------------------------------------------------
library(lubridate)
library(dplyr)
library(stringi)
library(RSQLite)             
library(mongolite)
library(testthat)

# Parse ARGs --------------------------------------------------------------
#-- Example would be: 
# Args = c("D:/WeihaoSheng/iftcoursework2021/WeihaoSheng/2.CourseworkTwo", "scripts.config", "scripts.params")
Args <- commandArgs(TRUE)
# Set working directory
setwd(Args[1])
# Source config
source(paste0("./config/", Args[2]))
# Source params
source(paste0("./config/", Args[3]))
# Source helper functions
source(Config$Directories$HelperFunctions)

printInfoLog("Main.R :: Script Settings Loaded...", type = "info")

# Set up MongoDB Connection -------------------------------------------
conMongo <- mongo(collection = Params$DataBase$Mongo$collection, db = Params$DataBase$Mongo$db, url = Params$DataBase$Mongo$url,
                  verbose = FALSE, options = ssl_options()) # creating connection from client to server... 

# Setting up connection for SQLite ----------------------------------------
conSql <- dbConnect(RSQLite::SQLite(), Config$Directories$SQLDataBase)

printInfoLog("Data connection....complete!")
# retrieve day11 and day12 --------------------------------------------------
source(Config$Directories$Day11and12)
Day11
Day12
printInfoLog("Data retrieving....complete!")

# find fat fingures day11 and 12 ---------------------------------
source(Config$Directories$fatfingerDay11)
source(Config$Directories$fatfingerDay12)
Day11mistake
Day12mistake
printInfoLog("Error person......fund!")

# insert error person to SQLite DB trades_suspects ---------------------
source(Config$Directories$Tablenew)

# aggregation quantity and notional----------------------------------
# The code below only works when insert new data to existing table
# if Day11 and Day12 already pre-exist, then insert Day 11 and Day12 will cause error
source(Config$Directories$aggregateDay11)
source(Config$Directories$aggregateDay12)

# test------------------------------------------------------
source(Config$Directories$test)

# Close Mongo & SQL Connections -------------------------------------------
dbDisconnect(conSql)
conMongo$disconnect()

printInfoLog("Main.R :: Script Completed")
# Script ends here --------------------------------------------------------


