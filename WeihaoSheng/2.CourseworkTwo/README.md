# UCL IFT - Big Data 2021

## Start Incorrect Trade Detection
This script will perform the following task:

1. Retrieve all trades for 2021-11-11 and 2021-11-12
2. Using trade vs price method to find mis-pricing on the trade
3. Create trades_suspects table into SQLite
4. aggregate Quantity and Notional for all trades by date, trader, symbol, ccy and insert the results into portfolio_positions

In order to trigger this script, from the command line/ shell / bash, please use:

```
cd ./iftcoursework2021/WeihaoSheng/2.CourseworkTwo
Rscript Main.R D:/WeihaoSheng/iftcoursework2021/WeihaoSheng/2.CourseworkTwo scripts.config scripts.params
```
Else, this can be run directly using R by hidden Args <- commandArgs(TRUE) and,
use Args = c("D:/WeihaoSheng/iftcoursework2021/WeihaoSheng/2.CourseworkTwo", "scripts.config", "scripts.params")

