#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author     : Bainuo Chen
# Coursework : Coursework3
# Use Case   : 3a
# File       : ui.R
#--------------------------------------------------------------------------------------
library(shiny)
library(lubridate)
library(DT)
library(lubridate)
library(dplyr)
library(RSQLite)   
library(mongolite)
library(dplyr)
library(tibble)
library(ggplot2)
library(shinydashboard)
options(scipen = 999)

# Source config
source(paste0("./BainuoChen/3.CourseworkThree/config/", "script.config"))
# Source params
source(paste0("./BainuoChen/3.CourseworkThree/config/", "script.params"))

#trader <- as.factor(sort(unique(portfolioPositionAll$trader)))


#-- create MongoDB connection
ConMongo <- mongo(DBConnection$MongoDataBase[[1]],DBConnection$MongoDataBase[[2]],DBConnection$MongoDataBase[[3]])

# Retrieve Data From MongoDB ------------------------------------------------------------------------------------------------------------------------------------------

# -- trader id
trader <- ConMongo$aggregate('[{"$group":{"_id":"$Trader"}}]')
colnames(trader) <- c("trader")
trader$trader <- sort(trader$trader)

ui <- dashboardPage(
  dashboardHeader(title = "Portfolio Information"),
  dashboardSidebar(
    selectizeInput('var1', 'Select a trader:', trader, selected = NULL),
    dateInput("var2","Select a trade date:",value="2020-01-06") #datedisabled
    #selectizeInput('var2', 'Select trade date',cob_date, selected = NULL)
  ),
  dashboardBody(
    fluidRow(
      tabBox(
        width =12,
        tabPanel("Main Portfolio Positions",DT::dataTableOutput("table1")),
        tabPanel("Main Exposures by Sector",DT::dataTableOutput("table2"),plotOutput("pie_chart",height="500px")) #plotlyOutput
      )
    )
  )
)
