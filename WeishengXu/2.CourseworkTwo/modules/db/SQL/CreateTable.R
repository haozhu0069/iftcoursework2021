#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Weisheng Xu
# Topic   : CourseworkTwo
#--------------------------------------------------------------------------------------

# SQL create a new table---------------------------------------------------------------

SQLCreateRetTable <- "CREATE TABLE trades_suspects (
  price_id TEXT NOT NULL PRIMARY KEY,
  open REAL NOT NULL,
  high REAL NOT NULL,
  low REAL NOT NULL
  close REAL NOT NULL,
  volumn INTEGER NOT NULL,
  currency TEXT NOT NULL,
  cob_date TEXT NOT NULL,
  Symbol TEXT NOT NULL,
  FOREIGN KEY (Symbol) REFERENCES equity_price(symbol_id))"
